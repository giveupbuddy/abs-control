#!/bin/bash

project_dir=$HOME/project
dcompose_dir=$HOME/abs-control

if ! test -f $project_dir/pom.xml; then
	echo "ERROR: pom.xml not found in $project_dir/ Check value of 'project_dir' in script variables!"
	exit 1
fi

if ! test -f $dcompose_dir/docker-compose.yml; then
	echo "ERROR: docker-compose.yml not found in $dcompose_dir/ Check value of 'dcompose_dir' in script variables!"
	exit 1
fi

sudo docker run -it --rm \
	--name absence-control \
	-v $project_dir:/usr/src/mymaven \
	-v $HOME/.m2:/root/.m2 \
	-w /usr/src/mymaven \
	maven:3.6.2-jdk-8-slim \
	mvn clean install

if test -f $project_dir/target/*.war; then
	echo "INFO: war-package found in $project_dir/target/            Starting containers..."
	cp $project_dir/target/*.war $dcompose_dir/web
else
	echo "ERROR: no war-package in $project_dir/target/ Possible build problems. Check logs"
	exit 1
fi

cd $dcompose_dir
sudo docker-compose stop
sudo docker-compose build
sudo docker-compose up -d

