#!/bin/bash

project_dir=$HOME/project

if ! test -f $project_dir/pom.xml; then
	echo "ERROR: pom.xml not found in $project_dir/ Check value of 'project_dir' in script variables!"
	exit 1
fi

sudo docker run -it --rm \
	--name absence-control \
	-v $project_dir:/usr/src/mymaven \
	-v $HOME/.m2:/root/.m2 \
	-w /usr/src/mymaven \
	maven:3.6.2-jdk-8-slim \
	mvn clean install

